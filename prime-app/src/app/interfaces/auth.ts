export interface User {
    id: string,
    name: string,
    lastnames: string;
    pass: string;
}
