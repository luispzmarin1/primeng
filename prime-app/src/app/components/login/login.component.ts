import { Component } from '@angular/core';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ButtonModule} from 'primeng/button';
import { RouterModule } from '@angular/router';
import { FormsModule, FormBuilder, Validators, ReactiveFormsModule} from '@angular/forms';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CardModule, InputTextModule, FormsModule, ReactiveFormsModule,
  PasswordModule, ButtonModule, RouterModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  reviewed=false;
  loginForm= this.fb.group({
  mail: ['', Validators.compose([Validators.required, Validators.email])],
  password: ['', Validators.required]
  })

  isMailValid(){
    return ! ( (this.loginForm.controls.mail.dirty  &&
        this.loginForm.controls.mail.invalid) ||
        this.loginForm.controls.mail.untouched);
  }

  isPassValid(){
    return (this.loginForm.value.password != "");
  }

  submitLoginForm(){
    if (this.isMailValid() && this.isPassValid()){
      alert("Todo valido")
    }else {
      this.reviewed = true;
    }
  }



  register(){
    alert("Register works")
  }

  constructor(private fb: FormBuilder){}

}
