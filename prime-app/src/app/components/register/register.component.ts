import { Component } from '@angular/core';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
import { RouterModule, Router } from '@angular/router';
import { FormsModule, FormBuilder, Validators, ReactiveFormsModule} from '@angular/forms';
import { AuthService } from '../../services/auth.service'
import { User } from '../../interfaces/auth';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-register',
  standalone: true,
  imports: [CardModule, InputTextModule, ButtonModule,
     RouterModule, FormsModule, ReactiveFormsModule,
     ToastModule],
  providers: [AuthService],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private messageService: MessageService,
    private routes:Router){}
  reviewed=false;
  registerForm= this.fb.group({
  name: ['', Validators.required],
  lastnames: ['', Validators.required],
  mail: ['', Validators.compose([Validators.required, Validators.email])],
  password: ['', Validators.required],
  repitPass: ['', Validators.required],
  })

  isMailValid(){
    return  ( (this.registerForm.controls.mail.valid));
  }

  isInputEmpty(inputName: string){
    return (this.registerForm.get(inputName)?.value != "");
  }

  isAnyInputEmpty(inputsNames: (string | null) []){
    for (let inputName of inputsNames){
      if(inputName === ("") ) { return false; }
    }
      return true;
  }

  submitRegisterForm(){
    if (this.isMailValid() && this.isAnyInputEmpty(Object.values(this.registerForm.getRawValue()))){
      alert("Todo valido")
      let registerUser = this.registerForm.value;
      delete registerUser.repitPass;
      this.authService.registerUser(registerUser as User).subscribe(
        response => {
          this.messageService.add({severity:'success', summary: 'Usuario creado correctamente', detail: ''});
          this.routes.navigate(['login']);
        },
        error => {
          this.messageService.add({severity:'error', summary: 'Ha habido un error', detail: 'Contacte con soporte'});
        }
        );
    }else {
      this.reviewed = true;
    }
  }

}
